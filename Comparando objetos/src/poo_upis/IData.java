package poo_upis;

public interface IData {
	boolean ehBissexto(int dia);
	byte getUltimoDia(int dia);
	
	int qualDia(int dia);
	int qualMes(int dia);
	
	byte getDia();
	void setDia(int dia);
	
	byte getMes();
	void setMes(int mes);
	
	short getAno();
	void setAno(int ano);
	
	void incrementaDia();
	void incrementaMes();
	void incrementaAno();
	
	void incrementaNDias(int numeroD);
	void incrementaNMes(int numeroM);
	void incrementaNAno(int numeroA);
	
	void print();
	
	boolean equals(Object obj);
}

