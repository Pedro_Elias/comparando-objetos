package poo_upis;

public class Horario implements IHorario{
	private int segundo;
		
	public Horario(IHorario hms) {
		this(hms.getSegundo(),
				hms.getHora(),
				hms.getMinuto());
	}
	
	public Horario(int i, int j, int k) {
		setSegundo(0);
	}
	public Horario(int segundo) {
		setSegundo(segundo);
	}

	public Horario(Horario horario) {
		// TODO Auto-generated constructor stub
		this(horario.getSegundo(),
				horario.getHora(),
				horario.getMinuto());
	}


	

	@Override
	public int getSegundo() {
		return segundo % 60;
	}
	@Override
	public void setSegundo(int segundo) {
		if(segundo >= 0 && segundo <= 59) {
			this.segundo = segundo;
		}
	}
	@Override
	public void setMinuto(int minuto) {
		if(minuto >= 0 && minuto <= 59) {
			this.segundo = ((segundo / 60) % 60);
		}
	}
	@Override
	public int getMinuto() {
		return ((segundo / 60) % 60);
	}
	@Override
	public void setHora(int hora) {
		if(hora >= 0 && hora <=23) {
			this.segundo = ((segundo / 3600) % 24);
		}
	}
	@Override
	public int getHora() {
		return ((segundo / 3600) % 24);
	}
	@Override
	public void adicionarSegundo() {
		if(segundo < 86400) {
			segundo = segundo + 1;
		}else {
			adicionarMinuto(segundo/60);
		}
	}
	@Override
	public void adicionarMinuto(int segundo) {
		if(segundo < 1440) {
			segundo = segundo + 60;
		}else {
			adicionarHora(segundo/3600);
		}
	}
	@Override
	public void adicionarHora(int segundo) {
		if(segundo < 24) {
			segundo = segundo + 3600;
		}
	}
	@Override
	public void adicionarNSegundos(int numeroS) {
		int i = 0;
		while(i < numeroS) {
			adicionarSegundo();
			i++;
		}
	}
	@Override
	public void adicionarNMinutos(int numeroM) {
		int i = 0;
		while(i<(numeroM*60)) {
			adicionarSegundo();
			i++;
		}
	}
	@Override
	public void adicionarNHora(int numeroH) {
		int i = 0;
		while(i<(numeroH*3600)) {
			adicionarSegundo();
			i++;
		}
	}
	@Override
	public boolean ehUltimoHorario() {
		return segundo == 86399;
	}
	@Override
	public boolean ehPrimeiroHorario() {
		return segundo == 0;
	}
	@Override
	public void print() {
		System.out.println(getHora() + " : " + getMinuto() + " : " + getSegundo());
	}
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null || obj.getClass()!=this.getClass()) {
			return false;
		}
		Horario h = (Horario) obj;
		return this.getHora() == h.getHora() && this.getMinuto() == h.getMinuto() && this.getSegundo() == h.getSegundo();
	}
	
}
