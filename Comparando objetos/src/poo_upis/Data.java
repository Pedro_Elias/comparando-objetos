package poo_upis;

public class Data {
	private int dia;

	private boolean ehBissexto(int dia) {
		return (dia % 146100 == 0 || ((dia % 36525 != 0) && (dia % 1461 == 0)));
	}
	
	private int getUltimoDia(int dia) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
		
		int m = qualMes(dia);
		if(m == 2 && ehBissexto(dia)) {
			return 29;
		}	
		return ud[m];
	}
	public byte qualDia(int dia) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
		int m = 0;
		while(dia>ud[m]) {
			dia -=ud[m];
			m++;
		}
		return (byte) dia;
	}
	public byte qualMes(int dia) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
		int m = 0;
		while(dia>ud[m]) {
			dia -=ud[m];
			m++;
		}
		return (byte) m;
	}
	
	public Data() {
		setDia((byte)1);
	}
	public Data(byte dia, byte mes, short ano) {
		this();
		setDia(dia);
	}
	public Data(int dia, int mes, int ano) {
		this((byte)dia, (byte)mes, (short)ano);
	}
	public int getDia() {
		return qualDia(dia);
	}
	
	public void setDia(int dia) {
		int ultimoDia = getUltimoDia(dia);
		if(dia >= 1 && dia <= ultimoDia) {
			this.dia = dia;
		}
	}
	
	public byte getMes() {
		return (byte) (qualMes(dia)%12);
	}
	
	public void setMes(int dia) {
		int mes = qualMes(dia);
		if(mes >= 1 && mes <= 12) {
			dia = mes;
		}
	}
	
	public short getAno() {
		return (short) (dia % 365);
	}
	public void setAno(int dia) {
		int ano = 0;
		if(qualMes(dia) > 12) {
			ano++;
		}
		if(ano >=1 && ano <= 9999) {
			dia = ano;
		}
	}
	
	
	
	public void incrementaDia() {
		byte d = (byte)(dia + 1);
		
		if(d > getUltimoDia(dia)) {
			dia = 1;
			incrementaMes();
		}else {
			setDia(dia);
		}
	}
	public void incrementaMes() {
			byte m = qualMes(dia);
			if(m > 12) {
				m = 1;
				incrementaAno();
			}else {
				setMes(m);
			}
	}
			
	public void incrementaAno() {
		int a = 0;
		if(qualMes(dia) > 12) {
			a++;
		}
		if(a > 9999) {
			a = 1;
		}else {
			setAno(a);
		}
	}
	
	public void incrementaNDias(int numeroD) {
		int i = 0;
		while(i < numeroD) {
			incrementaDia();
		}
	}
	public void incrementaNMes(int numeroM) {
		int i = 0;
		while(i < numeroM) {
			incrementaMes();
		}
	}
	public void incrementaNAno(int numeroA) {
		int i = 0;
		while(i < numeroA) {
			incrementaAno();
		}
	}
	
	public void print() {
		System.out.println( getDia()+ "\t/ " + getMes() + "\t/ " + getAno());
	}
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		Data d = (Data) obj;
		return this.getAno() == d.getAno() && this.getMes() == d.getMes() && this.getDia() == d.getDia();
	}
}
