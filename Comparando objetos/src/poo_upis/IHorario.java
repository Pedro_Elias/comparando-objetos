package poo_upis;

public interface IHorario {
	int getSegundo();
	void setSegundo(int segundo);
	
	int getMinuto();
	void setMinuto(int minuto);
	
	int getHora();
	void setHora(int hora);
	
	void adicionarSegundo();
	void adicionarMinuto(int segundo);
	void adicionarHora(int segundo);
	
	void adicionarNSegundos(int numeroS);
	void adicionarNMinutos(int numeroM);
	void adicionarNHora(int numeroH);
	
	boolean ehUltimoHorario();
	boolean ehPrimeiroHorario();
	
	void print();
	boolean equals(Object obj);
}

